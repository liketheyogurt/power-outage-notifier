# Power Outage Notifier

## The Big Picture

I want to create a product that can be plugged into the wall that will notify you when it has lost power. 

## How is it done?

The device will be a raspberry pi zero w. It will have a custome image that will be loaded on it. It will host a small web page that will allow for the user to connect to the internet and then allow them to log into/ create an account. Their account will be a paid account. The device will send **heart beats** to a web service. Once the web service stops receiving heart beats, it will notify the customer that there could be a power outage. Once power is restored and the service is receiving updates again, it will notify them of power restoration. 

## Features & Benefits
1. The electrical grid is unstable or breakers go bad and get tripped. This happens every day. In a struggling world, where food is expensive and inflation crushes the average american, we need to have a way to protect our food. Almost 100% of americans have a fridge, and 25% have more than one fridge or freezer. We have all had friends or family that have lost a freezer full of food because either the power went out or the breaker accidentily tripped. This can cost hundreds of dollars in wasted food. This device could save hundreds of dollars in food which could be feed your family for a very long time.
2. It is good to know when the power goes out in your house. There are many situations you'd want to be alerted when the power goes out. You could be on vacation and your pets are at home. Your elderly mother could be all alone during a 100 year freeze storm. Your family, pets, and food could be in danger when there is no electricity. Being able to react to those situations could save someones life or privide the means of living for your family.

## More on the Device

The rasperry pi zero w is a small, customizable, and potentially cheap product to produce. There is a lot of online support with the device. They are readily available online and are on hand now. The skills one would need to create a custom image are basic. The power draw on one is very low. It has the ability to connect to the internet. 

## More on the local host site

The custom image would come equipt with a lite web server. The web server would host a simple site developed in react. It would serve as an interface to allow the end user to connect to the internet. Once connected to the internet, it would allow the user to purchase an alerting package. Once signed up, it would then begin sending **heart beats** to the main service. the main service would send back a confirmation in the form of a 200. It would retry after so many seconds if the service didn't respond with a 200.

## More on the main service

The main service would be a paid service. It would receive **heart beats** from the unit on regular intervals. There would be a grace period given not all requests make it through. The main site would be a place where the user could update it's phone number, email address, update it's subscription, check history, or update wifi information.

# Questions that need answers

**How does a unit get reset?** The locally hosted site will always be running. One could reset the device by going there, logging in, and clicking on reset device. It would then remove the wifi settings, and delete the connection to the main server.
**How does one change the password?** The local hosted site will always be running. The information will be listed on the device will be printed on the device.
**What is the duration of the heart beat intervals?**
**Could one Change the wifi and or wifi password from the web ui?**
**Does the subscription cover multiple devices?**
**How to make money?** 
There are two ways to make money. 
1. Sales from the device. One would expect that the device would make money from it's sales. I would think that the profits on the devices will be small to make way for a larger subscription base. It is possible that the price of the item could be higher to make way for a free subscription that would notify within 24 hours vs a shorter period found from the paid subscription.
2. Subscriptions. Selling the device for a lower price could be a draw for a subscription service. The device would be useless without the subscription. The ongoing subscription for a simple service is where the majority of ongoing steady cash would come into play. 
**How often to send notifications?**
